# beginning of create_db.py
from models import app, db, Beer, Brewery, Style
import pandas

def create_brews():

    beers = pandas.read_csv('./datasets/beers_finalized.csv', encoding="ISO-8859-1")

    breweries = pandas.read_csv('./datasets/breweries_finalized.csv', encoding="ISO-8859-1")

    styles = pandas.read_csv('./datasets/styles_finalized.csv', encoding="ISO-8859-1")


    for index, oneBeer in beers.iterrows():
        _id = oneBeer['id']
        abv = oneBeer['abv']
        ounces = oneBeer['ounces']
        name = oneBeer['name']
        style = oneBeer['style']
        description = oneBeer['description']
        brewery = oneBeer['brewery_id']

        newBeer = Beer(id = _id, abv=abv, ounces=ounces, name=name, description=description, style=style, brewery=brewery)

        # After I create the book, I can then add it to my session.
        db.session.add(newBeer)
        # commit the session to my DB.
        db.session.commit()

    for index, oneBrewery in breweries.iterrows():
        _id = oneBrewery['id']
        name = oneBrewery['name']
        city = oneBrewery['city']
        state = oneBrewery['state']
        year = oneBrewery['year_established']

        newBrewery = Brewery(id = _id, name=name, city=city, state=state, year=year)

        # After I create the book, I can then add it to my session.
        db.session.add(newBrewery)
        # commit the session to my DB.
        db.session.commit()

    for index, oneStyle in styles.iterrows():
        _id = oneStyle['id']
        name = oneStyle['name']
        origin = oneStyle['origin']
        color = oneStyle['color']
        taste = oneStyle['taste']
        pop = oneStyle['popularity']

        newStyle = Style(id = _id, name=name, origin=origin, color=color, taste=taste, pop=pop)

        # After I create the book, I can then add it to my session.
        db.session.add(newStyle)
        # commit the session to my DB.
        db.session.commit()

create_brews()
# end of create_db.py
