# this is where the webapp will start

from flask import Flask, render_template, redirect, request, url_for
from flask_paginate import Pagination
from .create_db import app, db, Beer, Brewery, Style, create_brews
import requests
import subprocess

########
# HOME #
########

@app.route('/')
def start():
    return render_template('index.html')

#########
# BEERS #
#########

@app.route('/beers')
def beers():
    return redirect('/beers/1')

@app.route('/beers/<int:page>')
def beersPage(page=1):
    per_page = 25
    beers = db.session.query(Beer).paginate(page, per_page, error_out=False)

    return render_template('/beers/index.html', beers=beers, db=db, Brewery=Brewery, Style=Style)

@app.route('/beers/info/<int:beer_id>')
def beerGet(beer_id):
    beer = db.session.query(Beer).filter_by(id=beer_id).first()
    style = db.session.query(Style).filter_by(name=beer.style).first()
    return render_template('beers/info.html', beer=beer, db=db, Brewery=Brewery, Style=Style, style=style)

#############
# BREWERIES #
#############

@app.route('/breweries')
def breweries():
    return redirect('/breweries/1')

@app.route('/breweries/<int:page>')
def breweriesPage(page=1):
    per_page = 25
    breweries = db.session.query(Brewery).paginate(page, per_page, error_out=False)

    return render_template('breweries/index.html', breweries=breweries)

@app.route('/brewery/info/<int:brewery_id>')
def breweryGet(brewery_id):
    brewery = db.session.query(Brewery).filter_by(id=brewery_id).first()
    beers = db.session.query(Beer).filter_by(brewery=brewery_id).all()
    return render_template('breweries/info.html', brewery=brewery, beers=beers, Style=Style, db=db)

##########
# STYLES #
##########

@app.route('/styles')
def styles():
    return redirect('/styles/1')

@app.route('/styles/<int:page>')
def stylesPage(page=1):
    per_page = 25
    styles = db.session.query(Style).paginate(page, per_page, error_out=False)

    return render_template('styles/index.html', styles=styles)

@app.route('/style/info/<style_id>')
def styleGet(style_id):
    style = db.session.query(Style).filter_by(id=style_id).first()
    beers = db.session.query(Beer).filter_by(style=style.name).all()
    return render_template('styles/info.html', style=style, beers=beers, Brewery=Brewery, db=db)

##################
# ABOUT AND TEST #
##################

@app.route('/about')
def about():

	commits = getNumCommits(makeGitAPIRequest('repository/commits'))
	print(commits)

	issues = getNumIssues(makeGitAPIRequest('issues'))
	print(issues)

	context = [commits, issues]

	return render_template('about.html', context = context)

@app.route('/test/')
def test():
    p = subprocess.Popen(["coverage", "run", "--branch", "test.py"],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            stdin=subprocess.PIPE)
    out, err = p.communicate()
    output=err+out
    output = output.decode("utf-8") #convert from byte type to string type

    print(type("<br/>".join(output.split("\n"))))

    return render_template('test.html', output = "<br/>".join(output.split("\n")))

##########
# SEARCH #
##########

@app.route('/search/', methods=['GET', 'POST'])
def search():
    if request.method == "POST":
        query = request.form["query"]
        cls = request.form['class']

        if query == "" or query == None:
            return render_template("search.html", message="Need a search term, pal...")

        elif cls == "beer":
            results = db.session.query(Beer).filter(Beer.name.ilike("%"+query+"%")).all()
            return render_template('beers/index.html', beers=results, db=db, Brewery=Brewery, Style=Style, search=True)

        elif cls == "brewery":
            results = db.session.query(Brewery).filter(Brewery.name.ilike("%"+query+"%")).all()
            return render_template('breweries/index.html', breweries=results, search=True)

        elif cls == "style":
            results = db.session.query(Style).filter(Style.name.ilike("%"+query+"%")).all()
            return render_template('styles/index.html', styles=results, search=True)

    else:
        return render_template("search.html", message="")

##############
# GitLab API #
##############

def makeGitAPIRequest (_path):
	gitLabResponse = requests.get(url="https://gitlab.com/api/v4/projects/11301965/"+_path+"?private_token=pJ6xZre7VhgQeGzFcxEH&per_page=999")
	return gitLabResponse.json()

def getNumCommits(gitData):
	commits = {}

	for d in gitData:
		if d['author_name'] not in commits:
			commits[d['author_name']] = 1
		else:
			commits[d['committer_name']] += 1

	return commits

def getNumIssues(gitData):
	issues = {}

	for d in gitData:
		if d['author']['name'] not in issues:
			issues[d['author']['name']] = 1
		else:
			issues[d['author']['name']] += 1

	return issues
