import os
import sys
import unittest
from models import db, Beer, Brewery, Style
from create_db_test import create_brews

class DBTestCases(unittest.TestCase): 
	def test_source_beers_name1(self):
		r = db.session.query(Beer).filter_by(id = 14).one() 
		self.assertEqual(str(r.name), "Bitter Bitch")

	def test_source_beers_style1(self):
		r = db.session.query(Beer).filter_by(id = 14).one() 
		self.assertEqual(str(r.style), "American Pale Ale (APA)")

	def test_source_beers_id1(self):
		r = db.session.query(Beer).filter_by(id = 14).one() 
		self.assertEqual(str(r.id), "14")

	def test_source_beers_abv1(self):
		r = db.session.query(Beer).filter_by(id = 14).one() 
		self.assertEqual(str(r.abv), "0.061")

	def test_source_beers_ounces1(self):
		r = db.session.query(Beer).filter_by(id = 14).one() 
		self.assertEqual(str(r.ounces), "12")

	def test_source_beers_brewery1(self):
		r = db.session.query(Beer).filter_by(id = 14).one() 
		self.assertEqual(str(r.brewery), "177")

	def test_source_beers_description1(self):
		r = db.session.query(Beer).filter_by(id = 14).one() 
		self.assertEqual(str(r.description), "Nitpicking is unneeded from  this beer, as we prefer to let the hops provide all of our peer review. Hopped with Mosaic, Amarillo, and Warrior- and dry hopped with Mosaic.")

	def test_source_breweries_name1(self):
		r = db.session.query(Brewery).filter_by(id = 0).one() 
		self.assertEqual(str(r.name), "NorthGate Brewing ")

	def test_source_breweries_city1(self):
		r = db.session.query(Brewery).filter_by(id = 0).one() 
		self.assertEqual(str(r.city), "Minneapolis")

	def test_source_breweries_state1(self):
		r = db.session.query(Brewery).filter_by(id = 0).one() 
		self.assertEqual(str(r.state), " MN")

	def test_source_breweries_year1(self):
		r = db.session.query(Brewery).filter_by(id = 0).one() 
		self.assertEqual(str(r.year), "2012")

	def test_source_breweries_id1(self):
		r = db.session.query(Brewery).filter_by(id = 0).one() 
		self.assertEqual(str(r.id), "0")

	def test_source_styles_name1(self):
		r = db.session.query(Style).filter_by(id = 0).one() 
		self.assertEqual(str(r.name), "Abbey Single Ale")

	def test_source_styles_origin1(self):
		r = db.session.query(Style).filter_by(id = 0).one() 
		self.assertEqual(str(r.origin), "Belgium")

	def test_source_styles_color1(self):
		r = db.session.query(Style).filter_by(id = 0).one() 
		self.assertEqual(str(r.color), "Dark brown")

	def test_source_styles_taste1(self):
		r = db.session.query(Style).filter_by(id = 0).one() 
		self.assertEqual(str(r.taste), "Spicy peach, citrus marmalades and praline aromas and flavors with a pillowy dry-yet-fruity medium body and touch of wafer, sour apple and mossy river stones on the finish.")

	def test_source_styles_pop1(self):
		r = db.session.query(Style).filter_by(id = 0).one() 
		self.assertEqual(str(r.pop), "80")

	def test_source_styles_id1(self):
		r = db.session.query(Style).filter_by(id = 0).one() 
		self.assertEqual(str(r.id), "0")

if __name__ == '__main__': 
	unittest.main()
