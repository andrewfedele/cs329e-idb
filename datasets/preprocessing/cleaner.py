import pandas as pd

def remove_beers_1(beer_df):
    beer_df.fillna("None", inplace=True)
    for index, row in beer_df.iterrows():
        if row['description'].strip() == "No Description Provided" or row['description'].strip() == "Beer Not Found" or row['ibu'] == "None":
            beer_df.drop(index, inplace=True)

    return beer_df

def remove_breweries_1(beer_df, brewery_df):
    breweries_used = []
    for index, row in beer_df.iterrows():
        breweries_used.append(row['brewery_id'])

    for index, row in brewery_df.iterrows():
        if row['id'] not in breweries_used:
            brewery_df.drop(index, inplace=True)

    return brewery_df

def remove_styles_1(beer_df, style_df):
    styles_used = []
    for index, row in beer_df.iterrows():
        styles_used.append(row['style'])

    for index, row in style_df.iterrows():
        if row['name'] not in styles_used:
            style_df.drop(index, inplace=True)

    return style_df

###################################

def remove_breweries_2(brewery_df):
    for index, row in brewery_df.iterrows():
        if row['year_established'].strip() == "Brewery Not Found":
            brewery_df.drop(index, inplace=True)

    return brewery_df

def remove_beers_2(brewery_df, beer_df):
    breweries_used = []
    for index, row in brewery_df.iterrows():
        breweries_used.append(row['id'])

    for index, row in beer_df.iterrows():
        if row['brewery_id'] not in breweries_used:
            beer_df.drop(index, inplace=True)

    return beer_df

def remove_styles_2(beer_df, style_df):
    styles_used = []
    for index, row in beer_df.iterrows():
        styles_used.append(row['style'])

    for index, row in style_df.iterrows():
        if row['name'] not in styles_used:
            style_df.drop(index, inplace=True)

    return style_df

def main():
    beer_df = pd.read_csv('beers_updated.csv', encoding="ISO-8859-1")
    brewery_df = pd.read_csv('breweries_updated.csv', encoding="ISO-8859-1")
    style_df = pd.read_csv('styles.csv', encoding="ISO-8859-1")

    beer_df = remove_beers_1(beer_df)
    brewery_df = remove_breweries_1(beer_df, brewery_df)
    style_df = remove_styles_1(beer_df, style_df)

    #############################################################

    brewery_df = remove_breweries_2(brewery_df)
    beer_df = remove_beers_2(brewery_df, beer_df)
    style_df = remove_styles_2(beer_df, style_df)

    beer_df.to_csv("beers_finalized.csv")
    brewery_df.to_csv("breweries_finalized.csv")
    style_df.to_csv("styles_finalized.csv")

if __name__ == "__main__":
    main()
