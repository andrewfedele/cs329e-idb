import requests
import pandas as pd
import time
from bs4 import BeautifulSoup
import random

# Set URL bases
SEARCH_BASE = "https://www.brewerydb.com/search?q="
BEER_BASE = "https://www.brewerydb.com/beer/"
BREWERY_BASE = "https://www.brewerydb.com/brewery/"

# Scrape descriptions for beers
def add_beer_descriptions(beer_df):
    # For every beer in the data set
    for index, row in beer_df.iterrows():
        beer_name = row['name']
        beer_name_xml = beer_name.replace(' ', '+')
        print(beer_name)

        # Go to the search results page for that beer name
        request_url = (SEARCH_BASE + beer_name_xml)
        r = requests.get(request_url)
        soup = BeautifulSoup(r.content, 'lxml')

        try:
            # Get the BreweryDB ID for the beer
            soup = soup.find('div', id='beers')
            soup = soup.find('div', class_='product-name truncate')
            id = soup.find('a')['href']
            id = id.split('/beer/')[1]

            # Go to the beer's product page
            request_url = (BEER_BASE + id)
            r = requests.get(request_url)
            soup = BeautifulSoup(r.content, 'lxml')

            # Get the description
            desc = soup.find('div', class_='description-box')
            desc = list(desc.children)[1].get_text().strip()
        except:
            desc = "Beer Not Found"

        finally:
            # Add description to dataframe
            beer_df.loc[index, 'description'] = desc
            print(desc)
            print()

            # Sleep for a random interval, to reduce chance of bot detection
            time.sleep(random.randint(2, 5))

    return beer_df

# Scrape year established for breweries
def add_brewery_year_established(brewery_df):
    # For every brewery in the data set
    for index, row in brewery_df.iterrows():
        brewery_name = row['name'].strip()
        brewery_name_xml = brewery_name.replace(' ', '+')
        print(brewery_name)

        # Go to the search results page for that brewery name
        request_url = (SEARCH_BASE + brewery_name_xml)
        r = requests.get(request_url)
        soup = BeautifulSoup(r.content, 'lxml')

        try:
            # Get the BreweryDB ID for the brewery
            soup = soup.find('div', id='breweries')
            soup = soup.find('div', class_='product-name truncate')
            id = soup.find('a')['href']
            id = id.split('/brewery/')[1]

            # Go to the brewery page
            request_url = (BREWERY_BASE + id)
            r = requests.get(request_url)
            soup = BeautifulSoup(r.content, 'lxml')

            # Get the year established
            soup = soup.find('table', class_="table")
            soup = soup.find('td', text="Established:")
            soup = soup.find_next_sibling()
            year = soup.get_text()

        except:
            year = "Brewery Not Found"

        finally:
            # Add year to dataframe
            brewery_df.loc[index, 'year_established'] = year
            print(year)
            print()
            time.sleep(random.randint(2, 5))

    return brewery_df

def main():
    beer_df = pd.read_csv('beers.csv', encoding="ISO-8859-1")
    beer_df['description'] = ''
    beer_df = add_beer_descriptions(beer_df)
    beer_df.to_csv('beers_updated.csv')

    brewery_df = pd.read_csv('breweries.csv', encoding="ISO-8859-1")
    brewery_df['year_established'] = ''
    brewery_df = add_brewery_year_established(brewery_df)
    brewery_df.to_csv('breweries_updated.csv')


if __name__ == '__main__':
    main()
