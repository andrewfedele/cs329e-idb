import requests
import pandas as pd
import json
import time

BASEURL = "https://api.brewerydb.com/v2/"
APIKEY = "&key=79ecfa4054fad64c6abe81e6afa771c0"

def add_beer_descriptions(beer_df):
    for index, row in beer_df.iterrows():
        beer_name = row['name']
        beer_name = beer_name.replace(' ', '+')
        request_url = (BASEURL + 'search?q=' + beer_name + '&type=beer' + APIKEY)
        r = requests.get(request_url)
        response = json.loads(r.content)

        try:
            name = response['data'][0]['name'])
        except KeyError:
            name = 'Not Found'

        try:
            desc = response['data'][0]['description']
        except KeyError:
            desc 'No Description'

        if name != "Not Found":
            row['description'] = desc

        time.sleep(500)

def add_brewery_year_established(brewery_df):
    for index, row in breweries_df.iterrows():
        brewery_name = row['name']
        brewery_name = brewery_name.replace(' ', '+')
        request_url = (BASEURL + 'search?q=' + brewery_name + '&type=brewery' + APIKEY)
        r = requests.get(request_url)
        response = json.loads(r.content)

        try:
            name = response['data'][0]['name'])
        except KeyError:
            name = 'Not Found'

        try:
            year = response['data'][0]['established']
        except KeyError:
            year 'No Year'

        if name != "Not Found":
            row['year_established'] = year

        time.sleep(500)

def main():
    beer_df = pd.read_csv('beers.csv', encoding="ISO-8859-1")
    brewery_df = pd.read_csv('breweries.csv', encoding="ISO-8859-1")

    beer_df['description'] = ''
    brewery['year_established'] = ''

    beer_df = add_beer_descriptions(beer_df)
    brewery_df = add_brewery_year_established(brewery_df)

    beer_df.to_csv('beers_updated.csv')
    brewery_df.to_csv('breweries_updated.csv')


if __name__ == '__main__':
    main()
