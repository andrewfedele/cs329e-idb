# beginning of models.py
# note that at this point you should have created "bookdb" database (see install_postgres.txt).
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:asd123@localhost:5432/beerdb')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
db = SQLAlchemy(app)

class Beer(db.Model):
	# style (id), alcohol by volume, serving size, brewery, description
	__tablename__ = 'beer'
	
	name = db.Column(db.String(80))
	style = db.Column(db.String(80))
	abv = db.Column(db.Float)
	ounces = db.Column(db.Integer)
	brewery = db.Column(db.Integer)
	description = db.Column(db.String(5000))

	id = db.Column(db.Integer, primary_key = True)

class Brewery(db.Model):
	# name, city, state, brewery id, beer brewed, year established
	__tablename__ = 'brewery'
	
	name = db.Column(db.String(80))
	city = db.Column(db.String(80))
	state = db.Column(db.String(80))
	year = db.Column(db.Integer)

	id = db.Column(db.Integer, primary_key = True)

class Style(db.Model):
	# Country of Origin, Color, Taste, Example, Popularity (Int)
	__tablename__ = 'styles'
	
	name = db.Column(db.String(80))
	origin = db.Column(db.String(80))
	color = db.Column(db.String(80))
	taste = db.Column(db.String(1000))
	pop = db.Column(db.Integer)

	id = db.Column(db.Integer, primary_key = True)

db.drop_all()
db.create_all()
# End of models.py
